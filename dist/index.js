"use strict";

require("@babel/polyfill");

var _exchangeWrapper = _interopRequireDefault(require("./exchangeWrapper"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

module.exports = {
  GetExchange: function GetExchange(creds) {
    return new _exchangeWrapper["default"](creds);
  }
};