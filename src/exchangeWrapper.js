import Binance from './binance-api-node';

class ExchangeWrapper{
    
    constructor(params){
        var api = Binance(params);
        
        api.getExchangeUniverse = async function(){
            var dataset = await api.exchangeInfo();
            var data = [];
            if(dataset && dataset.symbols){
                data = dataset.symbols.map(item => {
                    return {
                        symbol: item.symbol,
                        name : item.name || item.symbol
                    }
                });
            };

            return data;
        }

        api.timeseries = async function(symbol, interval){
            var data = await api.candles({
                symbol : symbol,
                interval : interval,
                limit : 1000
            });
            return data;
        }

        return api;
    }
}

module.exports = ExchangeWrapper;

