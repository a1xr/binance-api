import "@babel/polyfill";

import ExchangeApi from './exchangeWrapper';


module.exports = {
    GetExchange : (creds) => { return new ExchangeApi(creds); }
};